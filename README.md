# Adobe PostScript

[[_TOC_]]

The story of PostScript has many different facets. It is a story about
profound changes in human literacy as well as a story of trade secrets within
source code. It is a story about the importance of teams, and of geometry. And
it is a story of the motivations and educations of engineer-entrepreneurs.

[Access the code here](src)

## The Big Picture

Printing has always been a technology with profound cultural
consequences. Movable type first emerged in east Asia, and, later, technology
from wine and oil presses in 15th century Europe was combined with novel
practices to mass produce type using metal casting to evolve the printing
press, and with it, a revolution in human literacy. Books became cheaper and
quicker to produce, and as a result appeared in ever greater numbers. Readers
and libraries expanded. Greater access to information transformed learning,
research, government, commerce, and the arts.

---

![](assets/Menuez_002-1024x667.jpg){width=25%}

*Adobe cofounders John Warnock (left) and Chuck Geschke (right).*

---

From the start of Adobe Systems Incorporated (now Adobe, Inc.) exactly forty
years ago in December 1982, the firm’s cofounders envisioned a new kind of
printing press—one that was fundamentally digital, using the latest advances
in computing. Initial discussions by cofounders Chuck Geschke and John Warnock
with computer-makers such as Digital Equipment Corporation and Apple convinced
them that software was the key to the new digital printing press. Their
vision: Any computer could connect with printers and typesetters via a common
language to print words and images at the highest fidelity. Led by Warnock,
Adobe assembled a team of skillful and creative programmers to create this new
language. In addition to the two cofounders, the team included Doug Brotz,
Bill Paxton, and Ed Taft. The language they created was in fact a complete
programming language, named PostScript, and was released by Adobe in 1984.

---

[![](https://img.youtube.com/vi/5xB_w5Nuwvg/hqdefault.jpg)](https://youtu.be/5xB_w5Nuwvg)

*Chuck Geschke discusses how Adobe came to focus on PostScript as their
initial business.*

---

By treating everything to be printed the same, in a common mathematical
description, PostScript granted abilities offered nowhere else. Text and
images could be scaled, rotated, and moved at will, as in the opening image to
this essay. Adobe licensed PostScript to computer-makers and printer
manufacturers, and the business jumped into a period of hypergrowth. There was
tremendous demand for the new software printing press. Computer-makers from
the established worlds of minicomputers and workstations to the rapidly
growing world of personal computers adopted the technology. Printer-makers
joined in, from well-established printers to the new laser printers and
professional typesetters. Software-makers rushed to make their offerings
compatible with PostScript.

Fueling this growth were advances Adobe was making around a critical need:
Providing professional-quality digital typefaces—and the many fonts that
comprise them—for use within PostScript. Adobe developed a fresh approach to
describing typefaces geometrically, and the company licensed many of the most
well-known typefaces, including those for Asian languages. PostScript and the
Adobe Type Library revolutionized printing and publishing, and kickstarted the
explosive growth of desktop publishing starting in the 1980s. PostScript
became so successful that it grew into a de facto standard internationally,
with Adobe publishing the details of the PostScript language, allowing others
to create products that were PostScript compatible. Today, most printers rely
on PostScript technology either directly or through a technology that grew out
of it: PDF (Portable Document Format).

---

![](assets/Trajan-Adobe-PostScript-resized.png){width=25%}

*An early typeface created by Adobe using its new technologies.*

---

John Warnock championed the development of PDF in the 1990s, transforming
PostScript into a technology that was safer and easier to use as the basis for
digital documents, but retaining all the benefits of interoperability,
fidelity, and quality. Over the decades, Adobe had developed PDF tremendously,
enhancing its features, and making it a crucial standard for digital
documents, printing, and for displaying graphics of all kinds on the screens
from laptops to smartphones and smartwatches.

Today, the digital printing press has far exceeded anything envisioned by the
Adobe cofounders when they first set out create PostScript with their
team. Almost everything printed on paper is done so using computers. Indeed,
in many areas of the world, computers have become the overwhelming tool for
writing. As Doug Brotz puts it, PostScript “democratized the print world.”
With PDF now so successful that it too has become a global standard, the
number of PDFs created each year is now measured in the trillions.

## A graphical background

Typography is the combination of art and technique that is concerned with the
display of writing, especially as printed. It is concerned with the shape and
placement of characters, words, paragraphs, and so on. In this, typography is
thoroughly graphical, a matter of visual design. Digital typography is no
different, just focused to computer techniques and displays. It is fitting,
then, that the roots of PostScript, and its contributions to the development
of digital typography, lie in advanced computer graphics.

Adobe cofounder John Warnock, the architect for PostScript, launched his
computing career as a graduate student at the University of Utah at the close
of the 1960s. Utah was then one of the world’s foremost centers for advanced
computer graphics research. In his work there, and then subsequently at a
leading computer graphics firm run by the lead professors at Utah, David Evans
and Ivan Sutherland, Warnock embraced their characteristic geometric approach
to computer graphics. Shapes, scenes, images, and animations were created and
designed by using mathematics to describe the geometry of the visual, and
using various computer procedures to realize these descriptions as imagery. In
particular, Warnock was impressed with the power of a procedural computer
language he and John Gaffney helped to develop at the firm Evans and
Sutherland, the “Design System.”

In 1978, Chuck Geschke had just set up a new organization in the famed Xerox
Palo Alto Research Center (PARC), the Imaging Science Laboratory. Geschke
hired Warnock into his laboratory, where Warnock took up a pressing challenge
for the lab. PARC was creating a set of new experimental computers with new
kinds of displays, and intended to be used with an array of novel printers—as
PARC had recently invented the laser printer. The challenge, which Warnock
took up, was to create a “device-independent” graphics system, which could be
used across any computer, display, or printer. To meet this challenge, Warnock
saw that something like the Design System could work if it were re-implemented
in this new computing environment, but refocused from 3D graphics to PARC’s
concern with professional quality printing and high-quality display of text
and images on displays. The result was another geometrical, procedural
language called JaM, that Warnock created in partnership with another PARC
researcher, Martin Newell.

---

![](assets/infiniti-postscript-swirl-resized.png){width=25%}

*An image made using JaM, the predecessor to PostScript.*

---

From 1979 into 1981, JaM became a major component in a new effort in Geschke’s
laboratory. This was a push to develop a printing language that could be
commercialized, used with the production version of PARC’s experimental
computers called the Xerox Star, and more broadly used across all of Xerox’s
lines of printers. A group of six Xerox researchers—Chuck Geschke, Butler
Lampson, Jerry Mendelson, Brian Reid, Bob Sproull, and John Warnock—melded the
JaM approach with other more established protocol techniques. The result was
named Interpress.

Xerox leadership was quickly convinced of the potential for Interpress,
deciding that it would indeed be developed into the firm’s printing
standard. However, moving to this standard would take several years during
which time Interpress would be under wraps. This delay in the further
refinement of Interpress by having it used and challenged more broadly outside
of Xerox spurred Geschke and Warnock to move. They would leave PARC to create
a startup in which they would create a rival to Interpress, but built more
fully along the geometric, procedural language approach that Warnock found to
be so powerful. But for the new startup to create this new language,
PostScript, as the digital printing press it would require a brilliant team.

---

[![](https://img.youtube.com/vi/_FI3LA6wV1g/hqdefault.jpg)](https://youtu.be/_FI3LA6wV1g)

*[Chuck Geschke discusses the motivations behind the formation of Adobe.](assets/102738493-05-01-acc.pdf)*

---

[![](https://img.youtube.com/vi/9S64_MgF_lQ/hqdefault.jpg)](https://youtu.be/9S64_MgF_lQ)

*John Warnock discusses key early actions in establishing Adobe.*

---

## PostScript's team

In December 1982, when Chuck Geschke and John Warnock created Adobe Systems
Incorporated, the new printing language they intended to create was at the
very center of their plans, hopes, and vision. The future of the firm hinged
on PostScript. Geschke and Warnock were themselves both highly experienced
software creators. Geschke had earned his Ph.D. at Carnegie Mellon University
working on advanced compilers, and had been a leader in the creation of an
important programming language developed and used at PARC called Mesa. As
discussed, Warnock had a Ph.D. in computer graphics software from the
University of Utah and years of experience creating languages exactly like
their envisioned PostScript. But even with, perhaps because of, their
extensive background in creating cutting-edge software, the cofounders knew
they needed to expand their team to create PostScript.

---

![](assets/Adobe-staff-on-boat-resized.png){width=25%}

*This photograph shows early Adobe employees and friends during a company
party, sailing in the San Francisco Bay. Many of them were welcomed by Chuck
Geschke (second from right in rear) and John Warnock (middle in rear) from
Xerox PARC, including Doug Brotz (leftmost rear), Dan Putman (third from left
in front), and Tom Boynton (right of Putman). Steve MacDonald (between Brotz
and Warnock) came from Hewlett-Packard to serve as Adobe’s first head of sales
and marketing. Linda Garger (third from right in front) was administrative
assistant for Geschke and Warnock and the first official Adobe
employee. Carolyn Bell (rightmost front) was an engineer. Marva Warnock, who
designed the first Adobe logo sits immediately in front of John Warnock. Nancy
Geschke, a librarian, sits in front and to the left of Chuck Geschke.*

---

Adobe’s PostScript team quickly took shape as three other highly talented
software creators from PARC decided to leave and rejoin with Geschke and
Warnock: Doug Brotz, Bill Paxton, and Ed Taft. Brotz had earned a Ph.D. in
computer science from Stanford before joining PARC in 1977, and became the
first computer scientist on the payroll, after the cofounders of
course. Paxton also had a Ph.D. in computer science from Stanford, and joined
PARC the same year as Brotz. Taft had joined PARC earlier, hired by Geschke as
soon as Taft finished his undergraduate studies at Harvard in 1973. Together,
and with input from company colleagues like Andy Shore, the team created
PostScript by the close of 1984.

## A trade secret in the source code

Adobe’s commitment to a geometrical approach for PostScript carried
consequences for how it would contend with typefaces—distinctive character
shapes—and the numerous fonts that actually realize these typefaces at
different sizes and styles (point sizes, regular, italic, bold, etc.). At
PARC, fonts had been created as a set of individual, hand-crafted bitmap
images—static definitions of which bits were on, and which were off, to create
each character of the font. In contrast, researchers at PARC and beyond were
exploring ways to define character shapes mathematically.

At Adobe, the team followed this mathematical description approach to fonts,
in keeping with the broader direction of PostScript, defining characters using
Bézier curves. But this still left the problem of device-independence. How
could Adobe’s font definitions contend with different displays, printers, and
different resolutions on both? For eyes so accustomed to reading published
text, even the slightest inconsistencies or irregularities in the appearance
of text are readily noticed and jarring. At lower resolutions, the chance for
these defects only becomes worse. Rendering fonts reliably at different
resolutions was a critical issue. Without a solution, PostScript could never
become the digital printing press.

---

![](assets/Adobe-secret-solution.png){width=25%}

*Elements of Adobe’s secret solution to creating professional quality fonts
for different resolutions.*

---

It was John Warnock who came up with Adobe’s solution, turning the problem
itself into the solution. The resolution of the output would determine a set
of procedures that would correct the fonts to optimize their appearance at
that resolution. Warnock, Brotz, and Paxton worked on the procedures for
months, eventually settling on ways to define key aspects of the font shapes,
and fitting them to the pixel rows and columns of the specified resolution,
and changing some aspects of the character shapes depending on the
resolution. Eventually, the Adobe team decided that greatest advantage lay in
keeping these approaches and procedures as a trade secret. They stayed secret
in PostScript’s source code, known to very few at the company, until Warnock
publicly disclosed them in a 2010 lecture.

The version of the PostScript source code released to the public by the
Computer History Museum is a very early version, dating to late
February 1984. While this version does contain an early version of the “font
hinting” procedures later kept as a trade secret, these approaches were
completely rewritten, expanded, and refined by Bill Paxton in subsequent
months. These changes were critical to the success of PostScript as it fully
came to market. Some modules that contain some low-level graphics engine
functions are not included in this release. Adobe still retains trade secret
rights in those modules. Otherwise, this collection is complete.

---

[![](https://img.youtube.com/vi/ry3nmWzsomQ/hqdefault.jpg)](https://youtu.be/ry3nmWzsomQ)

*Chuck Geschke discusses the trade secret in the PostScript source code.*

---
